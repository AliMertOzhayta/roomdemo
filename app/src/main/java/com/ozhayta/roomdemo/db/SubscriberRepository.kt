package com.ozhayta.roomdemo.db

class SubscriberRepository(private var dao: SubscriberDao) {

    val subscribers = dao.getAllSubscribers()


    suspend fun insert(subscriber: Subscriber) : Long {
        return dao.insertSubscriber(subscriber)
    }
    suspend fun insert(subscribers: List<Subscriber>) {
        dao.insertSubscribers(subscribers)
    }


    suspend fun update(subscriber: Subscriber) : Int {
        return dao.updateSubscriber(subscriber)
    }
    suspend fun update(subscribers: List<Subscriber>) {
        dao.updateSubscribers(subscribers)
    }


    suspend fun delete(subscriber: Subscriber) : Int {
        return dao.deleteSubscriber(subscriber)
    }
    suspend fun delete(subscribers: List<Subscriber>) {
        dao.deleteSubscribers(subscribers)
    }

    suspend fun deleteAll() : Int {
        return dao.deleteAll()
    }

}