package com.ozhayta.roomdemo.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface SubscriberDao {

    @Insert
    suspend fun insertSubscriber(subscriber: Subscriber) : Long // for returning id

    @Insert
    suspend fun insertSubscribers(subscribers: List<Subscriber>) : List<Long>
    /*
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun  insertSubscriber2(subscriber: Subscriber) : Long
     */

    @Update
    suspend fun updateSubscriber(subscriber: Subscriber) : Int

    @Update
    suspend fun updateSubscribers(subscribers: List<Subscriber>)


    @Delete
    suspend fun deleteSubscriber(subscriber: Subscriber) : Int // returning number of deleted subscribers

    @Delete
    suspend fun deleteSubscribers(subscribers: List<Subscriber>)


    @Query(value = "DELETE FROM subscriber_data_table")
    suspend fun  deleteAll() : Int

    // doesn't need coroutines that is why we didn't add suspend
    // no need for background tasks, just retrieving
    @Query(value = "SELECT * FROM subscriber_data_table")
    fun getAllSubscribers(): LiveData<List<Subscriber>>
}